using System;

namespace CopelandTech.Common.Attributes.ObjectManagement
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ObjectLifetimeAttribute : Attribute
    {
        private readonly Lifetime _objectLifetime;
        public ObjectLifetimeAttribute(Lifetime lifetime)
        {
            _objectLifetime = lifetime;
        }

        public Lifetime GetLifetime()
        {
            return _objectLifetime;
        }
    }
}