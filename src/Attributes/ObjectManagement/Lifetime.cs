namespace CopelandTech.Common.Attributes.ObjectManagement
{
    public enum Lifetime
    {
        Transient,
        Singletone
    }
}